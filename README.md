![Presentation image](./img/1.png)


# A classic bomberman

A simple bomberman projet realised in three weeks for my [Simplon](https://www.simplon.co "simplon.co") class. 
The goal was to learn the overall JS system with a cool and creative game.
I wrote it from scratch and used sprites from the Tigrounette boumboum game.

## Description

A very simple bomberman game with moves in the cardinal directions and bomb drops with spacebar.
The map is randomly generated with a specified width and height. Each block is separately generated.
The bonus system is added on "brick breaks". Two bonuses (bomb number and bomb power) are there and  for the moment skulls are useless.

## About Code

The most fun part was to setup a randomly generated map with editable specs.
The bomb explosion process was a little bit tricky for a beginner given the fact that the environnement is completely artificial.

That was also a first application of the Class system on a basic level, pretty cool !

## Future development

I would love to have more time on this project !
The game is working quite well, there is some small fixes to do and the last bonus to add (maybe changing speed of the player ?) but that's pretty much it. The bomb chain explosions is a big piece that could be very much fun to do.
Also adding a game objective would be interesting.

## License

Feel free to use all the code I've done !
All images belongs to [Atelier801](https://atelier801.com/index "Atelier801").

