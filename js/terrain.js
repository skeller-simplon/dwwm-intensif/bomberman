// The map is a an array of array delimited by the global.mapSizeX and global.mapSizeY


function generateMap(sizeX, sizeY) { // Launch the map making process
    let map = [];
    for (var i = 0; i < sizeX; i++) {
        map[i] = [];
    }
    for (let row = 0; row < sizeX; row++) {
        for (let column = 0; column < sizeY; column++) {
            createBlock(row, column, map, session.startingPosition);
        }
    }
    session.map = map;
};

function createBlock(row, column, map, startingPosition) { // Create a random block between air, brick & wall
    let number = Math.floor(Math.random() * 7);
    if (number >= 0 && number <= 2) {
        map[row][column] = {
            type: 'air',
            isBreakable: false,
            isMovable: true,
        }
        if (startingPosition === false) {
            player.posX = (row * global.gridSize);
            player.posY = (column * global.gridSize);
            startingPosition = true;
        }
    }
    else if (number >= 2 && number <= 5) {
        map[row][column] = {
            type: 'brick',
            isBreakable: true,
            isMovable: false,
        }
    }
    else if (number === 6) {
        map[row][column] = {
            type: 'wall',
            isBreakable: false,
            isMovable: false
        }
    }
    else {
        console.log('Error on block type - generateMap');
    }
}

function displayMap(map, sizeX, sizeY) { // Display all the map from the map tab of tab
    for (let row = 0; row < sizeX; row++) {
        for (let column = 0; column < sizeY; column++) {
            map[row][column].hasBomb = false;
            map[row][column].gridX = row;
            if (map[row][column].gridX === -0) {
                map[row][column].gridX = 0;
            }
            map[row][column].gridY = column;
            if (map[row][column].gridY === -0) {
                map[row][column].gridY = 0;
            }
            map[row][column].hasBonus = false;
            map[row][column].posX = (row * global.gridSize);
            map[row][column].posY = (column * global.gridSize);
            map[row][column].parent = '#map'
            create(map[row][column]);
        }
    }
};