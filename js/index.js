let global = {
    gridSize: 20,
    mapSizeX: 29, // 29
    mapSizeY: 19 // 19
}
let session = {
    startingX: 0,
    startingY: 0,
    map: undefined,
    startingPosition: false,
    counter: 0
}

startGame();

function startGame() { // Launch a new game by creating the map with the player in it
    let parent = document.querySelector('#canvas');
    let divMap = document.createElement("div");
    let divPlayer = document.createElement("div");
    divMap.id = "player";
    divPlayer.id = "map";
    parent.appendChild(divMap)
    parent.appendChild(divPlayer);

    displayStats(player.bombNumber, player.bombPower)
    generateMap(global.mapSizeX, global.mapSizeY);
    displayMap(session.map, global.mapSizeX, global.mapSizeY);
    create(player);
    player.gridX = player.posX / global.gridSize;
    player.gridY = player.posY / global.gridSize;
    let playerDisplay = document.querySelector(".player");
    playerDisplay.src = "../img/playerUp.png";
    if (session.counter < 11) {
        setInterval(function () {
            session.counter += 1;
        }, 1000)
    }
}

function reStartGame() { // basically relaunch a start + reset global variables
    player.bombPower = 1;
    player.bombNumber = 1;
    player.bombsDropped = [];
    session.map = []
    session.counter = 0;
    document.querySelector('#canvas').innerHTML = '';
    startGame();
}

/**
 * 
 * @param {Object} element Send an object
 * @returns {HTMLDivElement} 
 */
function create(element) { // Easily create an HTML element from an object
    let parent = document.querySelector(element.parent);
    let div = document.createElement("div");
    if (element.type === 'air' || element.type === 'brick' || element.type === 'wall' || element.type === 'playerBomb' || element.hasBonus !== false) {
        div.className = element.type + ' pos' + element.gridX + '-' + element.gridY;
    }
    if (element.type === 'player') {
        div.className = element.type;
    }
    div.style.height = global.gridSize + 'px';
    div.style.width = global.gridSize + 'px';
    parent.appendChild(div);
    div.style.left = element.posX + 'px';
    div.style.top = element.posY + 'px';
    return div;
};

/**
 * Remove an HTML element
 * @param {object} element Send and object
 */
function remove(element) { // removing an HTML from an object
    let michel = document.querySelector(element.parent)
    if (element.type === 'playerBomb') {
        session.map[element.gridX][element.gridY].hasBomb = false;
    }
    let itemRM = document.getElementsByClassName(element.type)

    if (element.type === 'air' || element.type === 'brick' || element.type === 'wall' || element.type === 'playerBomb' || element.hasBonus !== false) {
        itemRM = document.getElementsByClassName(element.type + ' pos' + element.gridX + '-' + element.gridY)
    }
    document.querySelector(element.parent).removeChild(itemRM[0]);
}

function displayStats(bombs, power) { // Used to display the HTML values of session.bombs & session.power
    document.querySelector("#bombs").innerHTML = "Nombre de bombes : " + player.bombsDropped.length + ' / ' + bombs;
    document.querySelector("#power").innerHTML = "Puissance des bombes : " + power;
};

// easy reseting games
document.querySelector("#button").addEventListener('click', function (e) {
    reStartGame();
})