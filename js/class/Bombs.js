class Bomb {

    /**
     * 
     * @param {number} posX PosX of the bomb
     * @param {number} posY PosY of the bomb
     * @param {number} power Detonation range of the bomb
     */
    constructor(posX, posY, power) {
        this.type = 'playerBomb';
        this.parent = '#canvas';
        this.posX = posX;
        this.posY = posY;
        this.gridX = posX / global.gridSize;
        this.gridY = posY / global.gridSize;
        this.power = power;
    };

    /**
     * 
     * @param {number} posX 
     * @param {number} posY 
     */
    add(map) { // Add a new bomb element in the tab
        map[this.gridX][this.gridY].hasBomb = true;
        create(this)
    }
    explode(map) { // Check the map in the 4 cardinals directions. Checks for walls, bricks to break, player to kill or others bombs to explode
        let bombsToExplodeLater = [];
        if (map[this.gridX][this.gridY].hasPlayer === true) {
            console.log('Player on bomb');
            if (session.counter > 10) {
                alert('GAME OVER')
                reStartGame();
            }
        }

        // Explosion Droite
        droite:
        for (let tempGridX = this.gridX + 1; tempGridX <= (this.gridX + this.power) && tempGridX < global.mapSizeX; tempGridX++) {
            if (map[tempGridX][this.gridY].type === 'wall') {
                break droite;
            }
            else if (map[tempGridX][this.gridY].isBreakable === true) {
                breakBlock(map[tempGridX][this.gridY])
                break droite;
            }
            else if (map[tempGridX][this.gridY].hasBomb === true) {
                if (bombFind(tempGridX, this.gridY) !== undefined) {
                    bombsToExplodeLater.push(bombFind(tempGridX, this.gridY));
                }
                else {
                    console.log('Cant find bomb');
                }
                break droite;
            }
            else if (map[tempGridX][this.gridY].hasPlayer === true) {
                console.log('Player detected on ' + tempGridX + ' ' + this.gridY);
                if (session.counter > 10) {
                    gameOver();
                }
            }
        }
        // Explosion Gauche
        gauche:
        for (let tempGridX = this.gridX - 1; tempGridX >= (this.gridX - this.power) && tempGridX >= 0; tempGridX--) {
            if (map[tempGridX][this.gridY].type === 'wall') {
                break gauche;
            }
            else if (map[tempGridX][this.gridY].isBreakable === true) {
                breakBlock(map[tempGridX][this.gridY])
                break gauche;
            }
            else if (map[tempGridX][this.gridY].hasBomb === true) {
                if (bombFind(tempGridX, this.gridY) !== undefined) {
                    bombsToExplodeLater.push(bombFind(tempGridX, this.gridY));
                }
                else {
                    console.log('Cant find bomb');
                }
                break gauche;
            }
            else if (map[tempGridX][this.gridY].hasPlayer === true) {
                console.log('Player detected on ' + tempGridX + ' ' + this.gridY);
                if (session.counter > 10) {
                    gameOver();
                }
            }
        }
        // Explosion Haut

        haut:
        for (let tempGridY = this.gridY + 1; tempGridY <= (this.gridY + this.power) && tempGridY < global.mapSizeY; tempGridY++) {
            if (map[this.gridX][tempGridY].type === 'wall') {
                break haut;
            }
            else if (map[this.gridX][tempGridY].isBreakable === true) {
                breakBlock(map[this.gridX][tempGridY])
                break haut;
            }
            else if (map[this.gridX][tempGridY].hasBomb === true) {
                if (bombFind(this.gridX, tempGridY) !== undefined) {
                    bombsToExplodeLater.push(bombFind(this.gridX, tempGridY));
                }
                else {
                    console.log('Cant find bomb');
                }
                break haut;
            }
            else if (map[this.gridX][tempGridY].hasPlayer === true) {
                console.log('Player detected on ' + this.gridX + ' ' + tempGridY);
                if (session.counter > 10) {
                    gameOver();
                }
            }
        }
        // Explosion Bas document.getElementById("myDIV").classList.add("mystyle");,  
        bas:
        for (let tempGridY = this.gridY - 1; tempGridY >= (this.gridY - this.power) && tempGridY >= 0; tempGridY--) {
            if (map[this.gridX][tempGridY].type === 'wall') {
                break bas;
            }
            else if (map[this.gridX][tempGridY].isBreakable === true) {
                breakBlock(map[this.gridX][tempGridY])
                break bas;
            }
            else if (map[this.gridX][tempGridY].hasBomb === true) {
                if (bombFind(this.gridX, tempGridY) !== undefined) {
                    bombsToExplodeLater.push(bombFind(this.gridX, tempGridY));
                }
                else {
                    console.log('Cant find bomb');
                }
                break bas;
            }
            else if (map[this.gridX][tempGridY].hasPlayer === true) {
                console.log('Player detected on ' + this.gridX + ' ' + tempGridY);
                if (session.counter > 10) {
                    gameOver();
                }
            }
        }
        remove(this);
        displayStats(player.bombNumber, player.bombPower);
        for (let i = 0; i < bombsToExplodeLater.length; i++) { // Have stored pos of chains-bombs, will explose them
            console.log(bombsToExplodeLater[i]);
            player.bombsDropped[bombsToExplodeLater[i]].explode;
        }
        player.bombsDropped.splice(bombFind(this.gridX, this.gridY), 1)
    }
}