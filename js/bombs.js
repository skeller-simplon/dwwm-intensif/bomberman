/**
 * 
 * @param {object} object Needs a bloc object that we need to remove
 */
function breakBlock(object) { // Break a block when a bomb explodes it
    tempGridX = object.posX / global.gridSize;
    tempGridY = object.posY / global.gridSize;
    remove(object);
    if (object.type === 'brick') {
        objet = addBonus(object)
    }
    else {
        object.type = 'air';
        object.isBreakable = false;
        object.isMovable = true;
        object.hasBomb = false;
        object.hasBonus = false;
    }
    object.gridX = tempGridX;
    object.gridY = tempGridY;
    object.parent = '#map'
    create(object);
};

/**
 * 
 * @param {number} matchingX 
 * @param {number} matchingy 
 */
function bombFind(matchingX, matchingy) { // find the bomb ID in the player.bombsDropped tab
    for (let i = 0; i < player.bombsDropped.length; i++) {
        if (player.bombsDropped[i].gridX === matchingX && player.bombsDropped[i].gridY === matchingy) {
            return i;
        };
    };
};

function addBonus(object) {// On grid element break, randomly add a bonus in it
    let bonusID = Math.floor(Math.random() * 5)
    if (bonusID === 0 || bonusID === 1) {
        object.type = 'air';
        object.isBreakable = false;
        object.isMovable = true;
        object.hasBomb = false;
        object.hasBonus = false;
        return object
    }
    else if (bonusID === 2) {
        object.type = 'bonusPower';
        object.hasBonus = 'power';
    }
    else if (bonusID === 3) {
        object.type = 'bonusBomb';
        object.hasBonus = 'bomb';
    }
    else if (bonusID === 4) {
        object.type = 'bonusSpecial';
        object.hasBonus = 'special';
    }
    object.isBreakable = true;
    object.isMovable = true;
    object.hasBomb = false;
    return object
}

function removeCarac(object) {// Used to remove carac
    remove(object);
    object.type = 'air';
    object.isBreakable = false;
    object.isMovable = true;
    object.hasBomb = false;
    object.hasBonus = false;
    create(object);
}

function gameOver() {
    alert('GAME OVER')
    reStartGame();
}
function callExplosion(element) {
    //x, y, i, power
    console.log(element);
    displayExplosion(element.gridX, element.gridY, 'start', 'x')
    for (let tempGridX = element.gridX + 1; tempGridX <= (element.gridX + element.power) && tempGridX < global.mapSizeX; tempGridX++) {
        if (tempGridX !== element.gridX + element.power) {
            displayExplosion(tempGridX, element.gridY, 'middle', 'x');
        }
        else {
            displayExplosion(tempGridX, element.gridY, 'end', 'x');
        }

    }
    for (let tempGridX = element.gridX - 1; tempGridX >= (element.gridX - element.power) && tempGridX >= 0; tempGridX--) {
        if (tempGridX !== element.gridX - element.power) {
            displayExplosion(tempGridX, element.gridY, 'middle', undefined);
        }
        else {
            displayExplosion(tempGridX, element.gridY, 'end', undefined);
        }
    }
    for (let tempGridY = element.gridY + 1; tempGridY <= (element.gridY + element.power) && tempGridY < global.mapSizeY; tempGridY++) {
        if (tempGridY !== element.gridY + element.power) {
            displayExplosion(element.gridX, tempGridY, 'middle', 'y');
        }
        else {
            displayExplosion(element.gridX, tempGridY, 'end', 'y');
        }
    }
    for (let tempGridY = element.gridY - 1; tempGridY >= (element.gridY - element.power) && tempGridY >= 0; tempGridY--) {
        if (tempGridY !== element.gridY - element.power) {
            displayExplosion(element.gridX, tempGridY, 'middle', 'z');
        }
        else {
            displayExplosion(element.gridX, tempGridY, 'end', 'z');
        }
    }
}
function displayExplosion(x, y, position, direction) {
    let div = document.querySelector('.pos' + x + '-' + y)
    let explosion = document.createElement('img');

    if (position === 'start') {
        explosion.src = "./img/explosionStart.png";
    }
    else if (position === 'middle') {
        explosion.src = "./img/explosionMid.png";
    }
    else {
        explosion.src = "./img/explosionEnd.png";
    }
    if (direction === 'x') {
        explosion.style.transform = 'rotate(180deg)';
    }
    else if (direction === 'y') {
        explosion.style.transform = 'rotate(270deg)';
    }
    else if (direction === 'z') {
        explosion.style.transform = 'rotate(90deg)';
    }


    div.appendChild(explosion);
    setTimeout(function () {
        div.innerHTML = '';
        return;
    }, 300);
}

// if (e.key == 'ArrowRight') {
//     divCaptur.style.left = divCaptur.offsetLeft + 5 + 'px';
//     divCaptur.style.transform = 'scaleX(-1)';
// } else if (e.key == 'ArrowLeft') {
//     divCaptur.style.left = divCapuur.offsetLeft + -5 + 'px';
//     divCaptur.style.transform = 'scaleX(1)';
// } else if (e.key == 'ArrowUp') {
//     divCaptur.style.top = divCaptur.offsetTop + -5 + 'px';
//     divCaptur.style.transform = 'rotate(90deg) scaleX(1)';
// } else if (e.key == 'ArrowDown') {
//     divCaptur.style.top = divCaptur.offsetTop + 5 + 'px';
//     divCaptur.style.transform = 'rotate(90deg) scaleX(-1)';
// }