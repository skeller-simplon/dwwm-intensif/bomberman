let player = {
    type: 'player',
    parent: '#player',
    posX: 0,
    posY: 0,
    bombNumber: 1,
    bombPower: 1,
    bombsDropped: [],
};

document.addEventListener('keydown', function (event) {
    if (event.code === 'ArrowUp') {
        event.preventDefault();
        if (isMovePossible(player.gridX, player.gridY - 1) === true) {
            session.map[player.gridX][player.gridY].hasPlayer = false;
            player.gridY -= 1;
            move(player).style.backgroundImage = "url('./img/playerUp.png')";
            if (session.map[player.gridX][player.gridY].hasBonus !== false) {
                checkBonus(session.map[player.gridX][player.gridY])
            }
        }
    }
    else if (event.code === 'ArrowDown') {
        event.preventDefault();
        if (isMovePossible(player.gridX, player.gridY + 1) === true) {
            session.map[player.gridX][player.gridY].hasPlayer = false;
            player.gridY += 1;
            move(player).style.backgroundImage = "url('./img/playerDown.png')";
            if (session.map[player.gridX][player.gridY].hasBonus !== false) {
                checkBonus(session.map[player.gridX][player.gridY])
            }
        }
    }
    else if (event.code === 'ArrowLeft') {
        event.preventDefault();
        if (isMovePossible(player.gridX - 1, player.gridY) === true) {
            session.map[player.gridX][player.gridY].hasPlayer = false;
            player.gridX -= 1;
            move(player).style.backgroundImage = "url('./img/playerLeft.png')";
            if (session.map[player.gridX][player.gridY].hasBonus !== false) {
                checkBonus(session.map[player.gridX][player.gridY])
            }
        }
    }
    else if (event.code === 'ArrowRight') {
        event.preventDefault();
        if (isMovePossible(player.gridX + 1, player.gridY) === true) {
            session.map[player.gridX][player.gridY].hasPlayer = false;
            player.gridX += 1;
            move(player).style.backgroundImage = "url('./img/playerRight.png')";
            if (session.map[player.gridX][player.gridY].hasBonus !== false) {
                checkBonus(session.map[player.gridX][player.gridY])
            }
        }
    }
    else if (event.code === 'Space') {
        event.preventDefault();
        if (player.bombsDropped.length < player.bombNumber && session.map[player.gridX][player.gridY].hasBomb === false) {
            let bombID = new Bomb(player.posX, player.posY, player.bombPower);
            player.bombsDropped.push(bombID)
            displayStats(player.bombNumber, player.bombPower);
            bombID.add(session.map)
            setTimeout(function () {
                setTimeout(function(){
                bombID.explode(session.map);
                }, 300)
                callExplosion(bombID)
            }, 3000);
        };
    };
});

function checkBonus(map) {// Check on move if a grid element has a bonus in it, if yes remove it and change player
    if (map.hasBonus === false) {
        return;
    }
    else if (map.hasBonus === 'bomb') {
        player.bombNumber += 1;
        removeCarac(map);
        displayStats(player.bombNumber, player.bombPower);
    }
    else if (map.hasBonus === 'power') {
        player.bombPower += 1;
        removeCarac(map);
        displayStats(player.bombNumber, player.bombPower);
    }
    else if (map.hasBonus === 'special') {
        console.log('Un jour faudra faire ce bonus');
        removeCarac(map);
    }
}

function isMovePossible(posX, posY) { // Check if a move is allowed in the posX & posY grid, check if .isMovable is true or false
    if (session.map[posX][posY].isMovable === true && session.map[posX][posY].hasBomb === false) {
        return true;
    }
    else {
        return false;
    }
}

function move(element) { //Remove an HTML an recreate it
    element.posX = element.gridX * global.gridSize;
    element.posY = element.gridY * global.gridSize;

    let player = document.querySelector('.' + element.type)
    player.style.left = element.posX + 'px';
    player.style.top = element.posY + 'px';
    session.map[element.posX / global.gridSize][element.posY / global.gridSize].hasPlayer = true;
    return player;
}